<?php
require "vendor/autoload.php";
require "class.php";

class ArvoreDiaboTest extends PHPUnit_Framework_TestCase
{
    public function testCriaArvoreNula()
    {
        $a = new ArvoreDiabo();
        $this->assertCount(0, $a->getNos());
    }

    public function testAcessarAtributosInexistentes()
    {
        $this->setExpectedException('AtributoInvalidoException');

        $a = new ArvoreDiabo();
        $no1 = $a->addNo(null);

        $this->assertEquals(1, $no1->naoexiste);
    }

    public function testAdicionaNos() {
        $a = new ArvoreDiabo();

        $no1 = $a->addNo(null);
        $nos = $a->getNos();

        $this->assertCount(1, $nos);
        $this->assertSame($no1, $nos[0]);
        $this->assertSame($no1, $a->getNo($no1->id));
        $this->assertInstanceOf('NoDiabo', $no1);

        $no2 = $a->addNo($no1);
        $nos = $a->getNos();

        $this->assertCount(2, $nos);
        $this->assertSame($no2, $nos[1]);
        $this->assertInstanceOf('NoDiabo', $no2);

        $this->assertSame($no2->pai, $no1);
        $this->assertNull($no1->pai);
    }

    public function testNaoPodeTerDuasRaizes()
    {
        $this->setExpectedException('NoInvalidoException');
        $a = new ArvoreDiabo();
        $no = $a->addNo(null);
        $no2 = $a->addNo(null);
    }

    public function testNoDentroDaArvores()
    {
        $this->setExpectedException('NoInexistenteException');

        $a1 = new ArvoreDiabo();
        $noA1 = $a1->addNo(null);

        $a2 = new ArvoreDiabo();
        $noA2 = $a2->addNo(null);

        $a1->addNo($noA2);
    }

    public function testRaizEsquerdaDireita()
    {
        $a = new ArvoreDiabo();
        $no1 = $a->addNo(null);
        $this->assertEquals(1, $no1->id);
        $this->assertEquals(1, $no1->left);
        $this->assertEquals(2, $no1->right);
    }

    public function testValidarEsquerdaDireita()
    {
        $a = new ArvoreDiabo();
        $no1 = $a->addNo(null);
        $no2 = $a->addNo($no1);


        $this->assertEquals(1, $no1->id);
        $this->assertEquals(1, $no1->left);
        $this->assertEquals(4, $no1->right);

        $this->assertEquals(2, $no2->id);
        $this->assertEquals(2, $no2->left);
        $this->assertEquals(3, $no2->right);

    }

    public function testAdicionarSegundoFilho()
    {
        $a = new ArvoreDiabo();
        $no1 = $a->addNo(null);
        $no2 = $a->addNo($no1);
        $no3 = $a->addNo($no1);

        $this->assertEquals(1, $no1->id);
        $this->assertEquals(1, $no1->left);
        $this->assertEquals(6, $no1->right);

        $this->assertEquals(2, $no2->id);
        $this->assertEquals(2, $no2->left);
        $this->assertEquals(3, $no2->right);

        $this->assertEquals(3, $no3->id);
        $this->assertEquals(4, $no3->left);
        $this->assertEquals(5, $no3->right);

    }

    public function testAdicionarTerceiroFilho()
    {
        $a = new ArvoreDiabo();
        $no1 = $a->addNo(null);
        $no2 = $a->addNo($no1);
        $no3 = $a->addNo($no1);
        $no4 = $a->addNo($no3);

        $this->assertEquals(1, $no1->id);
        $this->assertEquals(1, $no1->left);
        $this->assertEquals(8, $no1->right);

        $this->assertEquals(2, $no2->id);
        $this->assertEquals(2, $no2->left);
        $this->assertEquals(3, $no2->right);

        $this->assertEquals(3, $no3->id);
        $this->assertEquals(4, $no3->left);
        $this->assertEquals(7, $no3->right);

        $this->assertEquals(4, $no4->id);
        $this->assertEquals(5, $no4->left);
        $this->assertEquals(6, $no4->right);

    }

    public function testAdicionarFilhoDoMeio()
    {
        $a = new ArvoreDiabo();
        $raiz = $a->addNo(null);
        $filho1 = $a->addNo($raiz);
        $filho2 = $a->addNo($raiz);
        $filho3 = $a->addNo($raiz);
        $netoDo2 = $a->addNo($filho2);

        $this->assertEquals(1, $raiz->id);
        $this->assertEquals(1, $raiz->left);
        $this->assertEquals(10, $raiz->right);

        $this->assertEquals(2, $filho1->id);
        $this->assertEquals(2, $filho1->left);
        $this->assertEquals(3, $filho1->right);

        $this->assertEquals(3, $filho2->id);
        $this->assertEquals(4, $filho2->left);
        $this->assertEquals(7, $filho2->right);

        $this->assertEquals(4, $filho3->id);
        $this->assertEquals(8, $filho3->left);
        $this->assertEquals(9, $filho3->right);

        $this->assertEquals(5, $netoDo2->id);
        $this->assertEquals(5, $netoDo2->left);
        $this->assertEquals(6, $netoDo2->right);
    }

    public function testAdicionarComplexo()
    {
        $a = new ArvoreDiabo();
        $no1 = $a->addNo(null);
        $no2 = $a->addNo($no1);
        $no3 = $a->addNo($no1);
        $no4 = $a->addNo($no3);
        $no5 = $a->addNo($no1);
        $no6 = $a->addNo($no2);
        $no7 = $a->addNo($no3);
        $no8 = $a->addNo($no5);
        $no9 = $a->addNo($no7);
        $no10 = $a->addNo($no5);

        $this->assertEquals(1, $no1->id);
        $this->assertEquals(1, $no1->left);
        $this->assertEquals(20, $no1->right);

        $this->assertEquals(2, $no2->id);
        $this->assertEquals(2, $no2->left);
        $this->assertEquals(5, $no2->right);

        $this->assertEquals(3, $no3->id);
        $this->assertEquals(6, $no3->left);
        $this->assertEquals(13, $no3->right);

        $this->assertEquals(4, $no4->id);
        $this->assertEquals(7, $no4->left);
        $this->assertEquals(8, $no4->right);

        $this->assertEquals(5, $no5->id);
        $this->assertEquals(14, $no5->left);
        $this->assertEquals(19, $no5->right);

        $this->assertEquals(6, $no6->id);
        $this->assertEquals(3, $no6->left);
        $this->assertEquals(4, $no6->right);

        $this->assertEquals(7, $no7->id);
        $this->assertEquals(9, $no7->left);
        $this->assertEquals(12, $no7->right);

        $this->assertEquals(8, $no8->id);
        $this->assertEquals(15, $no8->left);
        $this->assertEquals(16, $no8->right);

        $this->assertEquals(9, $no9->id);
        $this->assertEquals(10, $no9->left);
        $this->assertEquals(11, $no9->right);

        $this->assertEquals(10, $no10->id);
        $this->assertEquals(17, $no10->left);
        $this->assertEquals(18, $no10->right);
    }


}