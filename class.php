<?php

class ArvoreDiabo
{
    private $nos;
    private $uniqueId;

    public function __construct()
    {
        $this->nos = array();
        $this->uniqueId = 1;
    }

    public function getNos()
    {
        return $this->nos;
    }

    public function getNo($id)
    {
        foreach ($this->nos as $no) {
            if ($no->id == $id) {
                return $no;
            }
        }

        return null;
    }

    public function addNo($noPai)
    {
        $no = new NoDiabo($noPai);
        $no->id = $this->getUniqueId();

        if ($noPai === null && $this->hasRoot()) {
            throw new NoInvalidoException;
        }

        if (!is_null($noPai)) {
            $noPaiInterno = $this->getNo($noPai->id);
            if ($noPaiInterno !== $noPai) {
                throw new NoInexistenteException;
            }

            $no->left = $noPai->right;
            $no->right = $no->left + 1;

            $this->atualizaNosADireitaDe($no->left);
        } else {
            $no->left = 1;
            $no->right = 2;
        }

        $this->nos[] = $no;

        return $no;
    }

    public function hasRoot()
    {
        foreach ($this->nos as $nozinho) {
            if ($nozinho->isRoot()) {
                return true;
            }
        }

        return false;
    }

    public function getUniqueId()
    {
        return $this->uniqueId++;
    }

    private function atualizaNosADireitaDe($left)
    {
        foreach ($this->nos as $no) {
            if ($no->left > $left) {
                $no->left += 2;
            }
            if ($no->right >= $left) {
                $no->right += 2;
            }
        }
    }
}

class NoDiabo
{
    private $props;

    public function __construct(NoDiabo $pai = null)
    {
        $this->props = [];

        $this->props['pai'] = $pai;
        $this->props['left'] = 1;
        $this->props['right'] = 2;
        $this->props['id'] = 1;
    }

    public function __get($propname)
    {
        if (array_key_exists($propname, $this->props)) {
            return $this->props[$propname];
        } else {
            throw new AtributoInvalidoException;
        }
    }

    public function __set($propname, $value)
    {
        if (array_key_exists($propname, $this->props)) {
            $this->props[$propname] = $value;
        } else {
            throw new AtributoInvalidoException;
        }
    }

    public function isRoot()
    {
        return is_null($this->pai);
    }
}

class NoInvalidoException extends Exception
{

}

class NoInexistenteException extends Exception
{

}

class AtributoInvalidoException extends Exception
{

}